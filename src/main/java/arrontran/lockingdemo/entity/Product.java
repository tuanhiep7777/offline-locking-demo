package arrontran.lockingdemo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Data
public class Product {

    @Id
    private Long id;

    private String name;

    private int quantity;

    private String version;
}
