package arrontran.lockingdemo.service;

import arrontran.lockingdemo.entity.Product;

public interface ProductService {

    Product buy1(Long id);
}
