package arrontran.lockingdemo.service.impl4;

import arrontran.lockingdemo.entity.Product;
import arrontran.lockingdemo.repository.ProductRepository;
import arrontran.lockingdemo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Qualifier("optimistic")
@Transactional
@AllArgsConstructor
public class OptimisticProductService implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public Product buy1(Long id) {

        Product product = productRepository.findById(id).orElse(null);
        if (product != null && product.getQuantity() > 0) {
            productRepository.updateQuantityWithVersion(id, product.getVersion());
        } else {
            throw new RuntimeException("Product not found/Not enough stock");
        }
        return null;
    }
}
