package arrontran.lockingdemo.service.impl5;

import arrontran.lockingdemo.entity.Product;
import arrontran.lockingdemo.repository.ProductRepository;
import arrontran.lockingdemo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Qualifier("serializable")
@Transactional
@AllArgsConstructor
public class SerializableProductService implements ProductService {

    private final ProductRepository productRepository;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Product buy1(Long id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product != null && product.getQuantity() > 0) {
            product.setQuantity(product.getQuantity() - 1);
            return productRepository.save(product);
        }

        throw new RuntimeException("Product not found/Not enough stock");
    }
}
