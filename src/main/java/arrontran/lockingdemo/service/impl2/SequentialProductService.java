package arrontran.lockingdemo.service.impl2;

import arrontran.lockingdemo.entity.Product;
import arrontran.lockingdemo.repository.ProductRepository;
import arrontran.lockingdemo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Qualifier("sequential")
@Transactional
@AllArgsConstructor
public class SequentialProductService implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public Product buy1(Long id) {
        productRepository.updateQuantity(id);
        return null;
    }
}
