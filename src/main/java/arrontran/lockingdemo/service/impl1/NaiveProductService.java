package arrontran.lockingdemo.service.impl1;

import arrontran.lockingdemo.entity.Product;
import arrontran.lockingdemo.repository.ProductRepository;
import arrontran.lockingdemo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Qualifier("naive")
@Transactional
@AllArgsConstructor
public class NaiveProductService implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public Product buy1(Long id) {

        Product product = productRepository.findById(id).orElse(null);
        if (product != null && product.getQuantity() > 0) {
            product.setQuantity(product.getQuantity() - 1);
            return productRepository.save(product);
        }

        throw new RuntimeException("Product not found/Not enough stock");
    }
}
