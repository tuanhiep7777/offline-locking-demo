package arrontran.lockingdemo.resource;

import arrontran.lockingdemo.entity.Product;
import arrontran.lockingdemo.service.ProductService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductResource {

    private final ProductService productService;

    public ProductResource(@Qualifier("optimistic") ProductService productService) {
        this.productService = productService;
    }

    @PutMapping("/buy/{id}")
    public Product buy1(@PathVariable Long id) {

        return productService.buy1(id);
    }
}
