package arrontran.lockingdemo.repository;

import arrontran.lockingdemo.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Modifying
    @Query("update Product p set p.quantity = p.quantity - 1 where p.id = ?1 and p.quantity > 0")
    void updateQuantity(Long id);

    @Query(value = "select p.* from Product p where p.id = ?1 for update", nativeQuery = true)
    Optional<Product> findByIdForUpdate(Long id);

    @Modifying
    @Query("update Product p set p.quantity = p.quantity - 1, p.version = cast(current_timestamp as text) where p.id = ?1 and p.quantity > 0 and p.version = ?2")
    void updateQuantityWithVersion(Long id, String version);
}
