package arrontran.lockingdemo;

import arrontran.lockingdemo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LockingdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LockingdemoApplication.class, args);
    }

}
